
//carregar Api
var api_key = "?api_key=f7f27958a6eb8140f6f0257025e1137b"
var url = "https://api.themoviedb.org/3"


var populares = "/movie/popular"
var detalhes = "/movie/{movie_id}"
var lasted = "/movie/latest"
var melhores = "/movie/top_rated"
var lancamentos = "/movie/now_playing"
var embreve = "/movie/upcoming"


// repositorio das imagens
img = "https://image.tmdb.org/t/p/w500"

//Filmes em Breve 

var request4 = new XMLHttpRequest();
request4.open('GET', url + embreve + api_key);
request4.onload = function () {
	if (request4.status >= 200 && request4.status < 400) {
		var data = JSON.parse(this.responseText);
		data2 = data.results.slice(0,5);
		
		var results2 = [results = {}];
		results2.results = data2;
		
		criar_HTML(results2);
		console.log(results2);

	}
	else{
		console.log("sem conexao com o servidor ");
	}	
};

request4.onerror = function(){
	console.log("Erro de conexao");
};

request4.send();

function criar_HTML(filmes_data) {
	var elemento = document.getElementById("filmes_em_breve-template").innerHTML;
	var compilar = Handlebars.compile(elemento);
	var gerar_html = compilar(filmes_data);

	var container = document.getElementById("breve_container");
	container.innerHTML = gerar_html;
}